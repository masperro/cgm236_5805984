﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostAI : MonoBehaviour {

    public Transform waypointsStart;
    public Transform waypointsSpawn;

    int cur = 0;
    int random = 0;
    public float speed = 0.3f;

    public bool moveUp = true;
    public bool moveDown;
    public bool moveLeft;
    public bool moveRight;

    void Update()
    {
        if(moveUp == true)
        {
            transform.position += new Vector3(0, speed, 0);
        }
        if (moveDown == true)
        {
            transform.position += new Vector3(0, -speed, 0);
        }
        if (moveLeft == true)
        {
            transform.position += new Vector3(-speed, 0, 0);
        }
        if (moveRight == true)
        {
            transform.position += new Vector3(speed, 0, 0);
        }

        random++;
        if(random > 3)
        {
            random = 0;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "4Way" && random ==0)
        {
            moveUp = true;
            moveDown = false;
            moveLeft = false;
            moveRight = false;
        }
        if (collision.gameObject.tag == "4Way" && random == 1)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;
            
        }
        if (collision.gameObject.tag == "4Way" && random == 2)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;
        }
        if (collision.gameObject.tag == "4Way" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;
           
        }

        if (collision.gameObject.tag == "3WayUp" && random == 0)
        {
            moveUp = true;
            moveDown = false;
            moveLeft = false;
            moveRight = false;
        }
        if (collision.gameObject.tag == "3WayUp" && random == 1)
        {
            moveUp = true;
            moveDown = false;
            moveLeft = false;
            moveRight = false;

        }
        if (collision.gameObject.tag == "3WayUp" && random == 2)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;
        }
        if (collision.gameObject.tag == "3WayUp" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;

        }

        if (collision.gameObject.tag == "3WayDown" && random == 0)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;
        }
        if (collision.gameObject.tag == "3WayDown" && random == 1)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;

        }
        if (collision.gameObject.tag == "3WayDown" && random == 2)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;
        }
        if (collision.gameObject.tag == "3WayDown" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;

        }

        if (collision.gameObject.tag == "3WayLeft" && random == 0)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;
        }
        if (collision.gameObject.tag == "3WayLeft" && random == 1)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;

        }
        if (collision.gameObject.tag == "3WayLeft" && random == 2)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;
        }
        if (collision.gameObject.tag == "3WayLeft" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;

        }

        if (collision.gameObject.tag == "3WayRight" && random == 0)
        {
            moveUp = true;
            moveDown = false;
            moveLeft = false;
            moveRight = false;
        }
        if (collision.gameObject.tag == "3WayRight" && random == 1)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;

        }
        if (collision.gameObject.tag == "3WayRight" && random == 2)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;
        }
        if (collision.gameObject.tag == "3WayRight" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;

        }

        if (collision.gameObject.tag == "2WayTL" && random == 0)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;
        }
        if (collision.gameObject.tag == "2WayTL" && random == 1)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;

        }
        if (collision.gameObject.tag == "2WayTL" && random == 2)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;


        }
        if (collision.gameObject.tag == "2WayTL" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;

        }

        if (collision.gameObject.tag == "2WayTR" && random == 0)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;
        }
        if (collision.gameObject.tag == "2WayTR" && random == 1)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;

        }
        if (collision.gameObject.tag == "2WayTR" && random == 2)
        {
            moveUp = false;
            moveDown = true;
            moveLeft = false;
            moveRight = false;


        }
        if (collision.gameObject.tag == "2WayTR" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;

        }

        if (collision.gameObject.tag == "2WayLL" && random == 0)
        {
            moveUp = true;
            moveDown = false;
            moveLeft = false;
            moveRight = false;
        }
        if (collision.gameObject.tag == "2WayLL" && random == 1)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;

        }
        if (collision.gameObject.tag == "2WayLL" && random == 2)
        {
            moveUp = true;
            moveDown = false;
            moveLeft = false;
            moveRight = false;


        }
        if (collision.gameObject.tag == "2WayLL" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;

        }

        if (collision.gameObject.tag == "2WayLR" && random == 0)
        {
            moveUp = true;
            moveDown = false;
            moveLeft = false;
            moveRight = false;
        }
        if (collision.gameObject.tag == "2WayLR" && random == 1)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;

        }
        if (collision.gameObject.tag == "2WayLR" && random == 2)
        {
            moveUp = true;
            moveDown = false;
            moveLeft = false;
            moveRight = false;


        }
        if (collision.gameObject.tag == "2WayLR" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;

        }

        if (collision.gameObject.tag == "2WayLeftRight" && random == 0)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;
        }
        if (collision.gameObject.tag == "2WayLeftRight" && random == 1)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;

        }
        if (collision.gameObject.tag == "2WayLeftRight" && random == 2)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = true;
            moveRight = false;


        }
        if (collision.gameObject.tag == "2WayLeftRight" && random == 3)
        {
            moveUp = false;
            moveDown = false;
            moveLeft = false;
            moveRight = true;

        }
    }
}
