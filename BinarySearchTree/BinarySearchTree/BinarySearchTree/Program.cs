﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    class Program
    {
        static void Main(string[] args)
        {
            BinarySearchTree nums = new BinarySearchTree();
            nums.Insert(42);
            nums.Insert(58);
            nums.Insert(24);
            nums.Insert(72);
            nums.Insert(69);
            nums.Insert(78);
            nums.Insert(6);


            //Console.WriteLine("BST : InOrder");
            //nums.InOrder(nums.root);

            //Console.WriteLine("BST : PreOrder");
            //nums.PreOrder(nums.root);

            //Console.WriteLine("BST : PostOrder");
            //nums.PostOrder(nums.root);

            //Console.WriteLine("Min Is :");
            //Console.WriteLine(nums.FindMin());

            Console.WriteLine("Mas Is :");
            Console.WriteLine(nums.FindMax());

            Console.Read();
        }
    }
}
